## [6.5.1] - 2019-08-12
### Added
- Listener for `timeupdate` to trigger jointime in some cases
### Library
- Packaged with `lib 6.5.11`

## [6.5.0] - 2019-05-31
### Library
- Packaged with `lib 6.5.2`

## [6.4.10] - 2019-03-26
### Fixed
- Error 3016 changed to nonfatal

## [6.4.9] - 2019-03-21
### Fixed
- Error 1002 changed to nonfatal
- Fixed seek reported instead of buffer after network restriction

## [6.4.8] - 2019-03-18
### Fixed
- Undefined monitor check

## [6.4.7] - 2019-02-19
### Added
- Error listener for the tag removed, not needed anymore
### Fixed
- Error severity detection
### Library
- Packaged with `lib 6.4.16`

## [6.4.6] - 2019-02-06
### Added
- Error listener for the player, not the tag
### Library
- Packaged with `lib 6.4.15`

## [6.4.5] - 2018-12-20
### Added
- New method to get resource
### Library
- Packaged with `lib 6.4.12`

## [6.4.4] - 2018-08-29
### Library
- Packaged with `lib 6.4.5`

## [6.4.3] - 2018-08-28
### Library
- Packaged with `lib 6.4.4`

## [6.4.2] - 2018-08-24
### Fix
- Fixed playrate for live content being reported as 0

## [6.4.1] - 2018-08-22
### Fix
- Fixed source getter
- Fixed isLive getter

## [6.4.0] - 2018-08-17
### Library
- Packaged with `lib 6.4.1`

## [6.3.0] - 2018-07-10
### Library
- Packaged with `lib 6.3.2`
### Added
- Support for shaka 1.x

## [6.2.0] - 2018-04-09
### Library
- Packaged with `lib 6.2.0`

## [6.1.0] - 2018-02-26
### Library
- Packaged with `lib 6.1.12`
